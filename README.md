![dotfiles](http://i.imgur.com/7zsMP5D.png?1)

The included .zshrc makes use of [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

### Git Aliases
For more detailson git aliases, view the [git.md](https://github.com/0x1A/dotfiles/blob/master/git.md)

Used Vim plugins:
* [NeoBundle](https://github.com/Shougo/neobundle.vim)
* [NeoComplete](https://github.com/Shougo/neocomplete.vim)
* [Nerdtree](https://github.com/scrooloose/nerdtree)
* [Vim-Airline](https://github.com/bling/vim-airline)
* [Solarized Color Scheme](https://github.com/altercation/vim-colors-solarized)
* [Syntastic](https://github.com/scrooloose/syntastic)
* [Vim-GitGutter](https://github.com/airblade/vim-gitgutter)
* [Vim-Multiple-Cursors](https://github.com/kristijanhusak/vim-multiple-curors)

## How to install
Run `install --<distro>`, where distro can be `arch, fed, or deb`.
### What it does
Installs vim, zsh, sets up NeoBundle, and installs vim plugins.

### What it doesn't
Doesn't change your shell
